<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>スッキリ商店</title>
</head>
<body>
<h1>スッキリ商店アカウントにログイン</h1>
<ul><span style="font-size: 24px;">
<li><a href="/SukkiriShop/LoginServlet">ログイン</a></li>
<p style="font-size:16px;">アカウントをお持ちの方はこちら</p>
<li><a href="">新規ユーザー登録</a></li>
<p style="font-size:16px;">初めて使用される方はこちら</p>
</span>
</ul>
</body>
</html>