package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import model.Account;
import model.Login;

public class AccountDAO {


    final String DRIVER_NAME = "com.mysql.jdbc.Driver";//MySQLドライバ
       final String DB_URL = "jdbc:mysql://localhost:3306/";//DBサーバー名
       final String DB_NAME = "sukkirishop";//データベース名
       final String DB_ENCODE = "?useUnicode=true&characterEncoding=utf8";//文字化け防止
       final String JDBC_URL =DB_URL+DB_NAME+DB_ENCODE;//接続DBとURL
       final String DB_USER = "root";//ユーザーID
       final String DB_PASS = "root";//パスワード


	public Account findByLogin(Login login) {
		Account account = null;

		try {
			Class.forName(DRIVER_NAME);
		} catch (ClassNotFoundException e1) {
			// TODO 自動生成された catch ブロック
			e1.printStackTrace();
		}



		try (Connection conn = DriverManager.getConnection(JDBC_URL, DB_USER, DB_PASS)) {

			String sql = "SELECT USER_ID,PASS,MAIL,NAME,AGE FROM ACCOUNT WHERE USER_ID=?AND PASS=?";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, login.getUserId());
			pStmt.setString(2, login.getPass());

			ResultSet rs = pStmt.executeQuery();

			if (rs.next()) {
				String userId = rs.getString("USER_ID");
				String pass = rs.getString("PASS");
				String mail = rs.getString("MAIL");
				String name = rs.getString("NAME");
				int age = rs.getInt("AGE");
				account = new Account(userId, pass, mail, name, age);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		return account;
	}

}